<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>img_SehatQ For Corporate_sc-jzJRlG dOVASO</name>
   <tag></tag>
   <elementGuidId>f843f132-8d6a-4c2c-90d8-d454f12b6186</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>img.sc-jzJRlG.dOVASO</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='popover-profile']/div/img</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>img</value>
      <webElementGuid>5233751f-80fb-4801-a6e2-c0104eac7766</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>alt</name>
      <type>Main</type>
      <value>Rizky</value>
      <webElementGuid>023ec57d-694a-4daa-8a04-58b21a7baf28</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>src</name>
      <type>Main</type>
      <value>https://static.sehatq.com/account/picture-empty/image-20220105-100017.png</value>
      <webElementGuid>80bb8879-58b2-4451-9897-4cdb4800dc4c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>height</name>
      <type>Main</type>
      <value>30</value>
      <webElementGuid>1214c92c-9ceb-4ea1-8726-eee51606bdee</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>width</name>
      <type>Main</type>
      <value>30</value>
      <webElementGuid>c63d3c42-1a46-425c-ab8d-f1433315f80c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>sc-jzJRlG dOVASO</value>
      <webElementGuid>b92d19f5-6257-4f1c-8641-b9197f5d58ec</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;popover-profile&quot;)/div[1]/img[@class=&quot;sc-jzJRlG dOVASO&quot;]</value>
      <webElementGuid>f38ca7b1-f954-4e34-9734-8968f4a76a1c</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='popover-profile']/div/img</value>
      <webElementGuid>985c2447-c7fe-4f12-bef4-9a99626cf599</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:img</name>
      <type>Main</type>
      <value>//img[@alt='Rizky']</value>
      <webElementGuid>333ddbdf-5d72-421d-a081-18a3013f2bf7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/img</value>
      <webElementGuid>a3946ee9-56b5-49a2-ab79-9928331df5fd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//img[@alt = 'Rizky' and @src = 'https://static.sehatq.com/account/picture-empty/image-20220105-100017.png']</value>
      <webElementGuid>33e48cda-b90c-4d7a-b5a9-0ff4fa318f89</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
