<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>textarea_Jalan jalan</name>
   <tag></tag>
   <elementGuidId>b0f9020c-0e2c-4042-83f3-991b2720a26e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#address</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//textarea[@id='address']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>textarea</value>
      <webElementGuid>9b31e18b-abe0-4f09-934a-dd41dc8c0a48</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>off</value>
      <webElementGuid>f89f4244-8389-45e8-ba9d-be1d852c67d6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>address</value>
      <webElementGuid>9347ec5b-b173-484d-b78a-33ce9f63c131</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>address</value>
      <webElementGuid>3d0c72b1-ddf9-4ed1-b1b1-64d97ceb9f66</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>rows</name>
      <type>Main</type>
      <value>4</value>
      <webElementGuid>9c8c7f13-7880-40c9-9695-3bde8063d255</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>sc-kTUwUJ kjLRID</value>
      <webElementGuid>54b0c219-21e6-4d35-b46d-3deb79fef15b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Jalan jalan</value>
      <webElementGuid>3c76349a-6c5d-43d5-9b7c-835cdd216434</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;address&quot;)</value>
      <webElementGuid>c350f9bf-5724-47a3-87e3-2a3de9834762</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//textarea[@id='address']</value>
      <webElementGuid>e479ece0-305f-4c07-8f0b-1360216e701f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='__next']/div/div/div/div[2]/div[2]/div[2]/div/div/div/div[8]/label/textarea</value>
      <webElementGuid>2d4c7bc7-35ef-4189-870a-79c8b4f38466</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Email'])[1]/following::textarea[1]</value>
      <webElementGuid>dc8424d0-a5db-4031-9312-e8828b0239f1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Verify'])[1]/following::textarea[1]</value>
      <webElementGuid>c3eb163f-3138-44a8-826f-37df1a2bdcb5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Alamat'])[1]/preceding::textarea[1]</value>
      <webElementGuid>5757dca9-16c6-4d57-ac29-1c21cc8f608c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Verifikasi Data Diri'])[1]/preceding::textarea[1]</value>
      <webElementGuid>226d1021-a548-498a-80bd-28bf7ae58f7d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Jalan jalan']/parent::*</value>
      <webElementGuid>f9fcb386-c1a3-45c1-83f9-5842bf30a46f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//textarea</value>
      <webElementGuid>3cf17efa-f595-4664-a94d-158ba2ec808f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//textarea[@id = 'address' and @name = 'address' and (text() = 'Jalan jalan' or . = 'Jalan jalan')]</value>
      <webElementGuid>db853447-235e-46da-9eec-1e56af3b26be</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
