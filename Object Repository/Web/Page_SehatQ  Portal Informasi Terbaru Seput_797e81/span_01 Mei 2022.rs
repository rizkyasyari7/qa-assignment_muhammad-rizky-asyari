<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_01 Mei 2022</name>
   <tag></tag>
   <elementGuidId>746f57c4-7e01-464e-9f3a-7e8ab5eb2589</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>span.sc-gZMcBi.frGFip</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='__next']/div/div/div/div[2]/div[2]/div/div/div[2]/div/div/div/div/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>94a084c5-bf9e-437b-a4cf-dcc2de4f8192</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>sc-gZMcBi frGFip</value>
      <webElementGuid>3a561f79-458f-44ed-b9b8-c3d420d37799</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>01 Mei 2022</value>
      <webElementGuid>b7d0b9f9-3ceb-409c-9e5a-9a02e78f3312</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;__next&quot;)/div[@class=&quot;sc-htpNat gIgHYH&quot;]/div[@class=&quot;sc-kGXeez bhTbbt&quot;]/div[@class=&quot;sc-dxgOiQ fHkFff&quot;]/div[@class=&quot;sc-ckVGcZ oGkLh&quot;]/div[@class=&quot;sc-htpNat hpMsZF&quot;]/div[@class=&quot;sc-htpNat fjMJVd&quot;]/div[@class=&quot;sc-dxgOiQ iZfmnq&quot;]/div[@class=&quot;sc-ckVGcZ kMzPHA&quot;]/div[@class=&quot;sc-dxgOiQ iZfmnq&quot;]/div[@class=&quot;sc-ckVGcZ kZOqcs&quot;]/div[@class=&quot;sc-htpNat fjMJVd&quot;]/div[@class=&quot;sc-bdVaJa crmNPM&quot;]/span[@class=&quot;sc-gZMcBi frGFip&quot;]</value>
      <webElementGuid>f98bd55e-31dd-4280-a323-e0fadb3f83df</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='__next']/div/div/div/div[2]/div[2]/div/div/div[2]/div/div/div/div/span</value>
      <webElementGuid>ca1c97ec-ce35-48ec-b9ce-4bb4bb230188</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Tanggal Lahir'])[1]/following::span[1]</value>
      <webElementGuid>37ef15a1-8aed-481c-8492-14544701ea91</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Berat (kg)'])[1]/following::span[2]</value>
      <webElementGuid>1858c441-2c08-47f7-aac1-38ee360efc65</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Jenis Kelamin'])[1]/preceding::span[1]</value>
      <webElementGuid>d6f91fd8-200d-40ec-beb6-af7aa5672ec2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Laki-laki'])[1]/preceding::span[2]</value>
      <webElementGuid>81768ba8-66d4-4d3b-820a-32ca0c150a97</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='01 Mei 2022']/parent::*</value>
      <webElementGuid>50f10988-2e6a-4e68-8742-b487834b4ab2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/div[2]/div/div/div/div/span</value>
      <webElementGuid>83612ccc-9ce8-42bb-8ec8-61366b6bd497</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = '01 Mei 2022' or . = '01 Mei 2022')]</value>
      <webElementGuid>e35dc837-fc69-4f69-8a94-ba0d40b392c3</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
