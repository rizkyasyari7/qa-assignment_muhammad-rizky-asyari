<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Jalan jalan</name>
   <tag></tag>
   <elementGuidId>b46a4cb6-d2fe-4c02-952c-c2f40f327577</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.sc-ckVGcZ.fOUpta > div.sc-htpNat.fjMJVd > div.sc-bdVaJa.crmNPM > span.sc-gZMcBi.frGFip</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='__next']/div/div/div/div[2]/div[2]/div/div/div[2]/div[3]/div/div/div/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>94a4ce0b-c779-412e-a538-75305b7e25aa</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>sc-gZMcBi frGFip</value>
      <webElementGuid>311968bb-3bd5-4970-9d0f-fa42bd1617f6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Jalan jalan</value>
      <webElementGuid>59120185-16a5-40da-a30d-cfe15d80987b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;__next&quot;)/div[@class=&quot;sc-htpNat gIgHYH&quot;]/div[@class=&quot;sc-kGXeez bhTbbt&quot;]/div[@class=&quot;sc-dxgOiQ fHkFff&quot;]/div[@class=&quot;sc-ckVGcZ oGkLh&quot;]/div[@class=&quot;sc-htpNat hpMsZF&quot;]/div[@class=&quot;sc-htpNat fjMJVd&quot;]/div[@class=&quot;sc-dxgOiQ iZfmnq&quot;]/div[@class=&quot;sc-ckVGcZ kMzPHA&quot;]/div[@class=&quot;sc-dxgOiQ jwcSKq&quot;]/div[@class=&quot;sc-ckVGcZ fOUpta&quot;]/div[@class=&quot;sc-htpNat fjMJVd&quot;]/div[@class=&quot;sc-bdVaJa crmNPM&quot;]/span[@class=&quot;sc-gZMcBi frGFip&quot;]</value>
      <webElementGuid>f49585e7-5abb-4655-8c86-c7e723d176cc</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='__next']/div/div/div/div[2]/div[2]/div/div/div[2]/div[3]/div/div/div/span</value>
      <webElementGuid>9e2062ae-8445-4711-a0a6-a2ded7faa24a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Alamat'])[1]/following::span[1]</value>
      <webElementGuid>90d5d533-54c5-4fbf-b9ec-a4684f83e6bf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='No. Telepon'])[1]/following::span[3]</value>
      <webElementGuid>c5a2c44d-401d-479b-b608-e0335012489b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Edit Profil'])[1]/preceding::span[1]</value>
      <webElementGuid>1c8f62d7-0f45-47ac-9f26-c1a76494a8e4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Jalan jalan']/parent::*</value>
      <webElementGuid>bba493b9-c25e-4181-b3c7-c68f0462e118</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div/div/span</value>
      <webElementGuid>f505dfd8-6c41-44a3-92c2-98def0a65219</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Jalan jalan' or . = 'Jalan jalan')]</value>
      <webElementGuid>2d7a77b1-2a65-48b8-8e08-d4825c8fdecc</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
