<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Berat Badan (kg)</name>
   <tag></tag>
   <elementGuidId>43dda945-dd65-42b3-adbf-df142901d02d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='__next']/div/div/div/div[2]/div[2]/div[2]/div/div/div/div[5]/div/label/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>79f771a6-9583-4816-ac13-e8247dbff96c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Berat Badan (kg)</value>
      <webElementGuid>156abd34-b69e-4b1c-846a-70227708e71c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;__next&quot;)/div[@class=&quot;sc-htpNat gIgHYH&quot;]/div[@class=&quot;sc-kGXeez bhTbbt&quot;]/div[@class=&quot;sc-dxgOiQ fHkFff&quot;]/div[@class=&quot;sc-ckVGcZ oGkLh&quot;]/div[@class=&quot;sc-dxgOiQ hTWzHs&quot;]/div[@class=&quot;sc-ckVGcZ ecObdJ&quot;]/div[@class=&quot;sc-htpNat mZALI&quot;]/div[@class=&quot;sc-htpNat jHFyUa&quot;]/div[@class=&quot;sc-htpNat fpKDVR&quot;]/div[@class=&quot;sc-htpNat hwkvNk&quot;]/div[@class=&quot;sc-eHgmQL jmRwVk&quot;]/label[@class=&quot;sc-jWBwVP fLPEuf&quot;]/span[1]</value>
      <webElementGuid>d49a7539-bcd9-4144-b6fd-5e6c3a699fa8</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='__next']/div/div/div/div[2]/div[2]/div[2]/div/div/div/div[5]/div/label/span</value>
      <webElementGuid>7d336c2d-f542-484b-969b-fb1ebe0fc562</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Tinggi Badan (cm)'])[1]/following::span[1]</value>
      <webElementGuid>9070bb9f-fd60-4028-bf58-4d23f536e533</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Jenis Kelamin'])[1]/following::span[2]</value>
      <webElementGuid>5fbe8210-fecb-44e9-abae-1e10fd86b84f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='No. Telepon'])[1]/preceding::span[1]</value>
      <webElementGuid>94ef5f5e-2fe1-4f48-b34e-d254b96b2507</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Verify'])[1]/preceding::span[2]</value>
      <webElementGuid>b8bceadc-7729-4f34-a279-0f3957b1086d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Berat Badan (kg)']/parent::*</value>
      <webElementGuid>97a7c68a-75f1-4af3-818f-46cdbc3e2ded</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[5]/div/label/span</value>
      <webElementGuid>c2d5ad97-4926-4b16-b91e-eb3db53aba0b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Berat Badan (kg)' or . = 'Berat Badan (kg)')]</value>
      <webElementGuid>13f05f16-694d-4779-a69b-6d81003e7aab</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
