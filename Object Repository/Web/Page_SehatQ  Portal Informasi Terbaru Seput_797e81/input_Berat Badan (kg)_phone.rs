<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Berat Badan (kg)_phone</name>
   <tag></tag>
   <elementGuidId>72487c30-1e2c-4557-b078-6af25bb95490</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>input[name=&quot;phone&quot;]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@name='phone']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>72a38df1-7754-4833-a6fb-e7af14090d04</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>59670a2a-e96d-49fc-b114-7bda935b5536</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>0</value>
      <webElementGuid>2fb51acd-788d-4685-82dd-8345b720c579</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>phone</value>
      <webElementGuid>b3791228-c285-4b16-b217-70a333cd9466</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>sc-gPEVay gJoBMb</value>
      <webElementGuid>f0d13160-2b31-4f4b-ab60-a189b737da56</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;__next&quot;)/div[@class=&quot;sc-htpNat gIgHYH&quot;]/div[@class=&quot;sc-kGXeez bhTbbt&quot;]/div[@class=&quot;sc-dxgOiQ fHkFff&quot;]/div[@class=&quot;sc-ckVGcZ oGkLh&quot;]/div[@class=&quot;sc-dxgOiQ hTWzHs&quot;]/div[@class=&quot;sc-ckVGcZ ecObdJ&quot;]/div[@class=&quot;sc-htpNat mZALI&quot;]/div[@class=&quot;sc-htpNat jHFyUa&quot;]/div[@class=&quot;sc-htpNat fpKDVR&quot;]/div[@class=&quot;sc-htpNat hwkvNk&quot;]/div[@class=&quot;sc-eHgmQL jmRwVk&quot;]/label[@class=&quot;sc-jWBwVP fbfWQj&quot;]/input[@class=&quot;sc-gPEVay gJoBMb&quot;]</value>
      <webElementGuid>9d2644ab-72c4-4e20-99b0-e77680cf9919</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@name='phone']</value>
      <webElementGuid>429dc6d3-9ea6-409a-9ede-5d07f1776922</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='__next']/div/div/div/div[2]/div[2]/div[2]/div/div/div/div[6]/div/label/input</value>
      <webElementGuid>7eacb908-d66b-48ec-8e67-f2c3a3a936ee</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[6]/div/label/input</value>
      <webElementGuid>bfb2ad10-7fa9-441c-b8dc-e266e3ab6cab</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'text' and @name = 'phone']</value>
      <webElementGuid>fa031fe6-25d9-4389-b089-821a815b6bc7</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
