<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Tinggi Badan (cm)</name>
   <tag></tag>
   <elementGuidId>225474cd-cf29-4740-a0c2-42ad6c02ae84</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='__next']/div/div/div/div[2]/div[2]/div[2]/div/div/div/div[4]/div/label/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>3d5b48db-d90c-42e4-8c3a-b231a95f922c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Tinggi Badan (cm)</value>
      <webElementGuid>526fcdf5-1149-40d7-a499-2fb13d05ca6e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;__next&quot;)/div[@class=&quot;sc-htpNat gIgHYH&quot;]/div[@class=&quot;sc-kGXeez bhTbbt&quot;]/div[@class=&quot;sc-dxgOiQ fHkFff&quot;]/div[@class=&quot;sc-ckVGcZ oGkLh&quot;]/div[@class=&quot;sc-dxgOiQ hTWzHs&quot;]/div[@class=&quot;sc-ckVGcZ ecObdJ&quot;]/div[@class=&quot;sc-htpNat mZALI&quot;]/div[@class=&quot;sc-htpNat jHFyUa&quot;]/div[@class=&quot;sc-htpNat fpKDVR&quot;]/div[@class=&quot;sc-htpNat hwkvNk&quot;]/div[@class=&quot;sc-eHgmQL jmRwVk&quot;]/label[@class=&quot;sc-jWBwVP fLPEuf&quot;]/span[1]</value>
      <webElementGuid>f6f427e4-e444-47f2-927d-6a36f4443fa7</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='__next']/div/div/div/div[2]/div[2]/div[2]/div/div/div/div[4]/div/label/span</value>
      <webElementGuid>3011f6ff-5bfe-494b-ae6b-b0172f618bbe</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Jenis Kelamin'])[1]/following::span[1]</value>
      <webElementGuid>ae4b0bc6-da29-4f94-bc9b-92dd3670fc90</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Laki-laki'])[1]/following::span[2]</value>
      <webElementGuid>dba76d30-ed9c-44cb-8ed8-e54565ecb403</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Berat Badan (kg)'])[1]/preceding::span[1]</value>
      <webElementGuid>9de4a53b-9e40-4dfb-8a7a-1eda7c7cdaf8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='No. Telepon'])[1]/preceding::span[2]</value>
      <webElementGuid>2e00fa6f-8181-4629-b3e5-24360750e46f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Tinggi Badan (cm)']/parent::*</value>
      <webElementGuid>c16ae528-2d15-4511-923a-4470072a728e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div/label/span</value>
      <webElementGuid>2a36546f-e627-4f18-b813-8a1d1b2a99b4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Tinggi Badan (cm)' or . = 'Tinggi Badan (cm)')]</value>
      <webElementGuid>c5a6a3b3-09d5-4fdd-856b-1a9008a6b584</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
