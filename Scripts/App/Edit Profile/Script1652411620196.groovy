import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration

Mobile.startApplication('C:\\Users\\ASUS\\Downloads\\SehatQ Tanya Dokter dan Booking Online Dokter_3.0.1_apkcombo.com.apk', 
    true)

Mobile.tap(findTestObject('Object Repository/App/android.widget.TextView - Halaman Utama'), 0)

Mobile.tap(findTestObject('Object Repository/App/android.widget.Button - Lewati'), 0)

Mobile.tap(findTestObject('Object Repository/App/android.widget.ImageView (1)'), 0)

Mobile.setText(findTestObject('Object Repository/App/android.widget.EditText - Email (1)'), 'rizkya@gmail.com', 0)

Mobile.setText(findTestObject('App/android.widget.EditText - Password (1)'), '123456', 0)

Mobile.tap(findTestObject('Object Repository/App/android.widget.Button - Log in'), 0)

Mobile.tap(findTestObject('Object Repository/App/android.widget.TextView - Lewati (1)'), 0)

Mobile.tap(findTestObject('Object Repository/App/android.widget.TextView - Ya (1)'), 0)

Mobile.tap(findTestObject('Object Repository/App/android.widget.TextView - Lihat Profil'), 0)

device_Height = Mobile.getDeviceHeight()

device_Width = Mobile.getDeviceWidth()

int startX = device_Width / 2

int endX = startX

int startY = device_Height * 0.30

int endY = device_Height * 0.70

Mobile.swipe(startX, endY, endX, startY)

Mobile.tap(findTestObject('Object Repository/App/android.widget.Button - Edit Profil'), 0)

Mobile.tap(findTestObject('App/android.widget.EditText - Tinggi Badan (cm)'), 0)

Mobile.tap(findTestObject('Object Repository/App/android.widget.EditText - Tanggal Lahir (1)'), 0)

Mobile.tap(findTestObject('App/android.widget.Button - Pilih'), 0)

Mobile.tap(findTestObject('Object Repository/App/android.widget.EditText - Perempuan'), 0)

Mobile.tap(findTestObject('App/android.widget.TextView - Laki-laki'), 0)

Mobile.setText(findTestObject('Object Repository/App/android.widget.EditText - Tinggi Badan (cm)'), '160', 0)

Mobile.setText(findTestObject('Object Repository/App/android.widget.EditText - Berat Badan (kg)'), '55', 0)

Mobile.swipe(startX, endY, endX, startY)

Mobile.setText(findTestObject('Object Repository/App/android.widget.EditText - No. Telepon'), '08123456789', 0)

Mobile.setText(findTestObject('Object Repository/App/android.widget.EditText - Alamat'), 'jln', 0)

Mobile.tap(findTestObject('Object Repository/App/android.widget.Button - Simpan'), 0)

Mobile.closeApplication()

